<?php
if(!function_exists('master_addon_categories')):
    function master_addon_categories( $elements_manager )
    {

        \Elementor\Plugin::$instance->elements_manager->add_category(
            'master-addons',
            [
                'title' => __('Master Elements ADDONS', 'masterelements'),
                'icon' => 'fa fa-plug',
            ]
        );
    }
endif;
if(!function_exists('master_init_widgets')):
    function master_init_widgets( ) {
    $widgets = [
        'plan-price',
        'call-of-action',
        'accordion',
        'heading',
        'info-box',
        'icon-box',
        'countdown',
        'post-title',
        'post-image',
        'post-content',
        'post-author-image',
        'breadcrumbs',
        'post-meta',
        'search',
    ];
    foreach ($widgets as $widget){
        require_once \MasterElements::widgets_dir().$widget.'/'.$widget.'.php';
        $class_name = '\Elementor\\'.\MasterElements\Classes\Utils::make_classname('Master_'.$widget);
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new $class_name() );
    }

}
endif;
