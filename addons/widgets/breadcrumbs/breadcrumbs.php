<?php
namespace Elementor;
use \Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit;
class Master_Breadcrumbs extends Widget_Base {
	public $base;
    public function get_name() {
		//get widget name from postbreadcrumbs-handler.php file
		return 'breadcrumbs';
	}
	//get widget title from postbreadcrumbs-handler.php file
    public function get_title() {
		return esc_html__( 'Master BreadCrumbs', 'masterelements' );
	}
	//get widget icon from postbreadcrumbs-handler.php file
    public function get_icon() {
		return 'breadcrumbs';
	}
	//get widget category from postbreadcrumbs-handler.php file
    public function get_categories() {
        return [ 'master-addons' ];
	}
	//register controls
    protected function _register_controls() {
		//$defaultColorsCode = \Elementor\Scheme_Color::get_default_scheme();
		//$defaultColorsCode = Scheme_Color::get_default_scheme();
		//Single Post BreadCrumbs section inside single post breadcrumbs widget
        $this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Single Post BraedCrumbs', 'masterelements' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		//alignment control inside Single Post BreadCrumbs section
		$this->add_control(
			'breadcrumbs_align',
			[
				'label' => __( 'Alignment', 'masterelements' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'masterelements' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'masterelements' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'masterelements' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'left',
				'selectors' => [
					'{{WRAPPER}} .master_breadcrumb' => 'text-align: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);
        $this->end_controls_section(); //end Single Post BreadCrumbs section
		//style section after Single Post BreadCrumbs section
		$this->start_controls_section(
			'style_section',
			[
				'label' => __( 'Style', 'masterelements' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		//typography control under style section
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'breadcrumbs_typography',
				'label' => __( 'Typography', 'masterelements' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .master_breadcrumb li',
			]
        );
        //Color picker control under style section
         $this->add_control(
        	'breadcrumbs_color',
        	[
        		'label' => __( 'BreadCrumbs Color Text', 'masterelements' ),
        		'type' => Controls_Manager::COLOR,
        		'scheme' => [
        			'type' => \Elementor\Scheme_Color::get_type(),
        			'value' => \Elementor\Scheme_Color::COLOR_3,
        		],
        		'default' =>  \Elementor\Scheme_Color::COLOR_3,
        		'selectors' => [
        			'{{WRAPPER}} .master_breadcrumb li , {{WRAPPER}} .master_breadcrumb li a' => 'color: {{VALUE}}',
        		],
        	]
		);
		$this->add_control(
        	'breadcrumbs_color_active',
        	[
        		'label' => __( 'BreadCrumbs Color Active', 'masterelements' ),
        		'type' => Controls_Manager::COLOR,
        		'scheme' => [
        			'type' => \Elementor\Scheme_Color::get_type(),
        			'value' => \Elementor\Scheme_Color::COLOR_3,
        		],
        		'default' =>  \Elementor\Scheme_Color::COLOR_3,
        		'selectors' => [
        			'{{WRAPPER}} .master_breadcrumb li.single-the-title ,{{WRAPPER}} .master_breadcrumb li.br-page-title' => 'color: {{VALUE}}',
				],
        	]
		);
		$this->add_responsive_control(
			'section-padding',
			[
				'label' => __( 'Padding', 'masterelements' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%', 'em' ],
				'selectors' => [
					'{{WRAPPER}} .master_breadcrumb > ul' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
        $this->end_controls_section(); //end style section
	}
	//front end rendering
    protected function render() {
		$settings = $this->get_settings_for_display();
		//get shortcodes file and display
		echo '<div class="master_breadcrumb"><ul><li style="display:inline-block"><a href="' . home_url() . '" rel="nofollow">Home</a></li>';
		if (is_category()){
			echo "<li class='spacer' style='display:inline-block'> &nbsp;&nbsp;&#187;&nbsp;&nbsp; </li>";
			the_category(' &bull; ');
		}
		if (is_single()){
			echo "<li class='spacer' style='display:inline-block'> &nbsp;&nbsp;&#187;&nbsp;&nbsp; </li><li class='single-the-title' style='display:inline-block' >";
			the_title();
			echo "</li>";
		}
		elseif (is_page()){
			echo "<li class='spacer' style='display:inline-block'> &nbsp;&nbsp;&#187;&nbsp;&nbsp; </li><li class='br-page-title' style='display:inline-block'>";
			echo the_title();
			echo "</li>";
		}
		elseif (is_search()){
			echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
			echo "<li style='display:inline-block'>";
			echo the_search_query();
			echo "</li>";
		}
		echo "</ul></div>";
  }
  
}