=== Master Elements ===
Contributors: devbunch
Tags: elements, addon,widgets,elementor
Donate link: #
Requires at least: 4.6
Tested up to: 5.4
Requires PHP: 5.6
Stable tag: 0.1
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Master Elements is a package of addon build for Elementor Page builder users which means Elementor Free or paid must be installed before Master Elements.   We aim to provide the best design for addons which are never provided

== Description ==
Master Elements is a package of addon build for Elementor Page builder users which means Elementor Free or paid must be installed before Master Elements.   We aim to provide the best design for addons which are never provided.
It has some most unique and powerful addons for Elementor page builder. please stay in touch for more addons

== Installation ==
Make sure you have installed Elementor (free or paid) before installing Master Elements

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the \\\'Plugins\\\' screen in WordPress
3. All Settings will be found inside Elementor
4. (Make your instructions match the desired user flow for activating and installing your plugin. Include any steps that might be needed for explanatory purposes)



== Frequently Asked Questions ==
= Do we need Elementor plugin to use Master Elements ?

Yes you need Elementor pro or free to use this Master Elements addon

= How to use Master Elements ? =

After installing Master Elements , you will find all the new add-on under Elementor side panel inside Master Elements Category


= Elementor editor fails to load or not working? =
It\'s due to your server PHP settings. Increase your server PHP memory limit from the wp-config.php file or php.ini file.  If you don\'t have an idea about it. Please contact your hosting provider and ask to increase
* PHP memory_limit = 512M
* max_execution_time = 300


== Screenshots ==
1. dashboard
2. addon
3. options

== Changelog ==
Version 0.1
First stable Release of plugin

== Upgrade Notice ==
Wordpress 4.9+