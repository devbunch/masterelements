

<!DOCTYPE html>
<html>
<head>
	<title>EM Dashboard</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<div class="outer-box">

	<div class="top-wellcobox">
		<h1>Welcome to <span>Master Elements</span></h1>
		<img src="<?php echo  \MasterElements::assets_url() .'images/logo.png' ?>" alt="Logo"/>
	</div>
	<div class="activate-box" style="display:none;">
		<p>Master Elements Not Activated! to Unlock All Features Activate Now.</p>
		<div class="active-nowbtn">
			<a href="#">Activate Now</a>
		</div>
	</div>
	<div class="tabs">
		<button onclick="showDashboardTabClass()" class="dropbtn">&#9776;</button>
		<div id="admin-tabs" class="admin-tabs-navs">
			<button class="admin-tab-item active" onclick="openDashboardTab('Dashboard')">Dashboard</button>
			<button class="admin-tab-item " onclick="openDashboardTab('Customization')">Customization</button>
			<button class="admin-tab-item " onclick="openDashboardTab('Demo-Importer')">Demo Importer</button>
			<button class="admin-tab-item " onclick="openDashboardTab('Template-Kits')">Templates</button>
			<button class="admin-tab-item " onclick="openDashboardTab('Plugins')">Plugins</button>
			<button class="admin-tab-item " onclick="openDashboardTab('Tutorials')">Tutorials</button>
			<button class="admin-tab-item " onclick="openDashboardTab('Feedback')">Feedback</button>
			<button class="admin-tab-item " onclick="openDashboardTab('Systep-Status')">System Status</button>
			<button class="admin-tab-item " onclick="openDashboardTab('Update')">Update</button>
		</div>

	</div>

	<div id="Dashboard" class="tabcontent">

		<div class="outer-section">

			<div class ="col">

				<div class="option-boxes">


					<figure><img src="<?php echo  \MasterElements::assets_url() .'images/need-some-help.png' ?>" alt="need help image"/></figure>
					<h3>Need Some Help?</h3>
					<p>We would love to be of any assistance.</p>
					<div class="option-btn">
						<a target="_blank" href="https://wordpress.org/support/plugin/masterelements/">Send Ticket</a>

					</div>
				</div>

			</div>

			<div class ="col">

				<div class="option-boxes">

					<figure><img src="<?php echo  \MasterElements::assets_url() .'images/documentation-img.png' ?>" alt="documenation image"/></figure>
					<h3>Documentation</h3>
					<p>learn about any aspect of Master Elements Theme.</p>
					<div class="option-btn">
						<a target="_blank" href="https://masterelements.com/knowledgebase/">Start Reading</a>

					</div>
				</div>
			</div>
			<div class ="col">
				<div class="option-boxes">

					<figure><img src="<?php echo  \MasterElements::assets_url() .'images/Subscription-img.png' ?>" alt="subscription image"/></figure>
					<h3>Subscription</h3>
					<p>Get the latest changes in your inbox.</p>
					<div class="option-btn">
						<a href="#">Coming Soon</a>

					</div>

				</div>

			</div>

			<!-- EM OUTER SECTION -->
		</div>

		<!-- Dashboard Div Section end  -->

	</div>

	<div id="Customization" class="tabcontent">

		<div class="outer-section">

			<div class ="col padding-0">
				<div class="option-boxes coming-soon">
					<h2>Cooming Soon</h2>
				</div>
			</div>

			<!-- EM OUTER SECTION -->
		</div>

		<!-- Customization div section end -->

	</div>

	<div id="Demo-Importer" class="tabcontent">

		<div class="outer-section">

			<div class ="col padding-0">
				<div class="option-boxes coming-soon">
					<h2>Cooming Soon</h2>
				</div>
			</div>

			<!-- EM OUTER SECTION -->
		</div>

		<!-- Demo-Importer div section end -->

	</div>

	<div id="Template-Kits" class="tabcontent">

		<div class="outer-section">

			<div class ="col padding-0">
				<div class="option-boxes coming-soon">
					<h2>Cooming Soon</h2>
				</div>
			</div>

			<!-- EM OUTER SECTION -->
		</div>

		<!-- Template-Kits div SECTION end -->

	</div>
	<div id="Plugins" class="tabcontent">
		<h2>Activated Plugins</h2>
		<div class="outer-section">
					<?php
					$active_plugins = (array) get_option( 'active_plugins', array() );

					if ( is_multisite() ) {
						$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
					}
					foreach ( $active_plugins as $plugin ) {
						$plugin_data    = @get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin );
						$dirname        = dirname( $plugin );
						$version_string = '';
						$network_string = '';
						if ( ! empty( $plugin_data['Name'] ) ) {
							// link the plugin name to the plugin url if available
							$plugin_name = esc_html( $plugin_data['Name'] );
							if ( ! empty( $plugin_data['PluginURI'] ) ) {
								$plugin_name = '<a href="' . esc_url( $plugin_data['PluginURI'] ) . '" title="' . __( 'Visit plugin homepage' , 'masterelements' ) . '" target="_blank">' . $plugin_name . '</a>';
							}
							?>
							<div class ="col">

								<div class="option-boxes">
									<h3><?php echo $plugin_name; ?></h3>
									<p><?php echo sprintf( _x( 'by %s', 'by author', 'masterelements' ), $plugin_data['Author'] ) . ' Version &ndash; ' . esc_html( $plugin_data['Version'] ) . $version_string . $network_string; ?></p>
								</div>

							</div>
							<?php
						}
					}
					?>
			<!-- EM OUTER SECTION -->
		</div>

		<!-- Plugins DIV  SECTION end -->

	</div>
	<div id="Tutorials" class="tabcontent">

		<div class="outer-section">

			<div class ="col padding-0">
				<div class="option-boxes coming-soon">
					<h2>Cooming Soon</h2>
				</div>
			</div>
			<!-- EM OUTER SECTION -->
		</div>

		<!-- Tutorials DIV  SECTION end -->


	</div>
	<div id="Feedback" class="tabcontent">

		<div class="outer-section">

			<div class ="col padding-0">
				<div class="option-boxes coming-soon">
					<h2>Cooming Soon</h2>
				</div>
			</div>

			<!-- EM OUTER SECTION -->
		</div>

		<!-- Feedback DIV  SECTION end -->

	</div>

	<div id="Systep-Status" class="tabcontent">
		<div class="section-content-box">
			<h3 class="content-title"><?php _e('Some informaition about your WordPress installation which can be helpful for debugging or monitoring your website.', 'masterelements' ); ?></h3>
			<div class="status-wrapper">

				<table class="table" cellspacing="0">
					<thead>
					<tr>
						<th colspan="3"><?php _e( 'WordPress Environment', 'masterelements' ); ?></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><?php _e( 'Home URL', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php echo home_url(); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'Site URL', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php echo site_url(); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Version', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php bloginfo('version'); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Multisite', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php if ( is_multisite() ) echo '&#10004;'; else echo '&#10005;'; ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Memory Limit', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php
							// This field need to make some changes
							$server_memory = 0;
							if( function_exists( 'ini_get' ) ) {
								echo ( ini_get( 'memory_limit') );
							}
							?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Permalink', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php  echo get_option( 'permalink_structure' ); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'WP Debug Mode', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php if ( defined('WP_DEBUG') && WP_DEBUG ) echo '<mark class="yes">' . '&#10004;' . '</mark>'; else echo '<mark class="no">' . '&#10005;' . '</mark>'; ?></td>
					</tr>
					<tr>
						<td><?php _e( 'Language', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php echo get_locale() ?></td>
					</tr>
					</tbody>
				</table>

				<table class="table" cellspacing="0">
					<thead>
					<tr>
						<th colspan="3"><?php _e( 'Server Environment', 'masterelements' ); ?></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><?php _e( 'Server Info', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php echo esc_html( $_SERVER['SERVER_SOFTWARE'] ); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'PHP Version', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php
							// should add the cpmparsion check for version_compare(PHP_VERSION, '5.0.0', '<')
							if ( function_exists( 'phpversion' ) ) echo esc_html( phpversion() ); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'Server Info', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php echo esc_html( $_SERVER['SERVER_SOFTWARE'] ); ?></td>
					</tr>
					<?php if ( function_exists( 'ini_get' ) ) : ?>
						<tr>
							<td><?php _e( 'PHP Post Max Size', 'masterelements' ); ?>:</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><?php _e( 'PHP Time Limit', 'masterelements' ); ?>:</td>
							<td></td>
							<td><?php
								$time_limit = ini_get('max_execution_time');
								//should add the condition
								if ( $time_limit < 60 && $time_limit != 0 ) {
									echo '<mark class="server-status-error">' . sprintf( __( '%s - We recommend setting max execution time to at least 60. See: <a href="%s" target="_blank">Increasing max execution to PHP</a>', 'masterelements' ), $time_limit, 'http://codex.wordpress.org/Common_WordPress_Errors#Maximum_execution_time_exceeded' ) . '</mark>';
								} else {
									echo '<mark class="yes">' . $time_limit . '</mark>';
								}
								?>
							</td>
						</tr>
						<tr>
							<td><?php _e( 'PHP Max Input Vars', 'masterelements' ); ?>:</td>
							<td></td>
							<td><?php echo ini_get('max_input_vars'); ?></td>
						</tr>
						<tr>
							<td><?php _e( 'SUHOSIN Installed', 'masterelements' ); ?>:</td>
							<td></td>
							<td><?php echo extension_loaded( 'suhosin' ) ? '&#10004;' : '&#10005;'; ?></td>
						</tr>
					<?php endif; ?>
					<tr>
						<td><?php _e( 'MySQL Version', 'masterelements' ); ?>:</td>
						<td></td>
						<?php
						/** @global wpdb $wpdb */
						global $wpdb;
                      //  $DB = $wpdb->db_version();
						//echo $DB;
                        _e( $wpdb->db_version(), 'masterelements' );
						?>
						</td>
					</tr>
					<tr>
						<td><?php _e( 'Max Upload Size', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php echo size_format( wp_max_upload_size() ); ?></td>
					</tr>
					<tr>
						<td><?php _e( 'Default Timezone is UTC', 'masterelements' ); ?>:</td>
						<td></td>
						<td><?php
							$default_timezone = date_default_timezone_get();
							if ( 'UTC' !== $default_timezone ) {
								echo '<mark class="server-status-error">' . '&#10005; ' . sprintf( __( 'Default timezone is %s - it should be UTC', 'masterelements' ), $default_timezone ) . '</mark>';
							} else {
								echo '<mark class="yes">' . '&#10004;' . '</mark>';
							} ?>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!-- Status update DIV  SECTION end -->

	</div>
	<div id="Update" class="tabcontent">

		<div class="outer-section">

			<div class ="col padding-0">
				<div class="option-boxes coming-soon">
					<h2 class="already-update-plugin">Master Elements is Upto date.</h2>
					<h2 class="update-plugin-required" style="display:none;">A new version of Master Elements is available <a class="update-now"  type="button"  href="#">Update now</a></h2>
				</div>
			</div>

			<!-- EM OUTER SECTION -->
		</div>

		<!-- Updates DIV  SECTION end -->

	</div>
	<div class="log-badge">
		<h3>Change Log</h3>

		<!-- LOG BADGE -->
	</div>

	<div class="version-message">

		<!-- <pre><span>Version 5.2.13</span> (02.12.2019)</pre>
       <pre><span>Version 5.2.11</span> (02.12.2019)</pre>
       <p><span>New</span> -->

		<?php
		$filename= MEROOT."/changelog.txt";
		$fp = fopen($filename, 'r');

		// Add each line to an array
		if ($fp) {
			$fcontent = explode("\n", fread($fp, filesize($filename)));
		}
		$i = 1;
		// echo '<pre>'.print_r($fcontent,true).'</pre>';
		foreach($fcontent as $str){
			//echo strcmp($str,'end');

			if($i==1){
				echo '<p><span class="date">Date</span>'.$str.'</p>';

			}
			elseif($i==2){
				$st = explode(' ',$str);
				echo '<p><span class="version">Version</span>'.$st[1].'</p>';

			}
			else{
				// echo $str;
				if(strpos($str, "[New]") !== false)
				{
					$str=str_replace("[New]", "",$str);
					echo '<p><span class="new">New</span>'.$str.'</p>';
				}
				elseif(strpos($str, "[Update]") !== false){
					$str=str_replace("[Update]", "",$str);
					echo '<p><span class="update">Update</span>'.$str.'</p>';
				}
				elseif(strpos($str, "[Fix]") !== false){
					$str=str_replace("[Fix]", "",$str);
					echo '<p><span class="fix">Fix</span>'.$str.'</p>';
				}
			}
			$i++;

		}
		?>

	</div>
</body>
</html>