<?php
defined( 'ABSPATH' ) || exit;
/**
 * Plugin Name: MasterElements
 * Description: The most advanced addons for Elementor.
 * Plugin URI: https://akdesigner.com/
 * Author: TeamDevBunch
 * Version: 0.1
 * Author URI: https://devbunch.com/
 *
 * Text Domain: masterelements
 *
 * @package MasterElements
 * @category Free
 *

 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
register_activation_hook(__FILE__, 'master_activate');

function master_activate() {
    if ( file_exists( WP_PLUGIN_DIR . '/elementor/elementor.php' ) && is_plugin_active( WP_PLUGIN_DIR . '/elementor/elementor.php')) {
        add_option('me_do_activation_redirect', true); //add option for redirection
    }
}
function master_redirect_to_dashboard() { //dashboard redirection function
    if (get_option('me_do_activation_redirect', false)) {
        delete_option('me_do_activation_redirect');
    }

}
define( 'MEROOT', plugin_dir_path( __FILE__ ) );
if(!class_exists('MasterElements')):
    final class MasterElements{

        const MINIMUM_ELEMENTOR_VERSION = '2.4.0';
        const PACKAGE_TYPE = 'free';
        const version = '0.1';
        const MINIMUM_PHP_VERSION = '5.6';

        static function plugin_dir(){
            return trailingslashit(plugin_dir_path( __FILE__ ));
        }
        private static $_instance = null;
        public static function instance() { //create one instance of class (singleton)
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }
        static function plugin_url(){
            return trailingslashit(plugin_dir_url( __FILE__ ));
        }

        static function admin_dir(){
            return self::plugin_dir() . 'admin/';
        }
        static function admin_url(){
            return self::plugin_url() . 'admin/';
        }
        static function assets_url(){
            return self::plugin_url() . 'assets/';
        }
        static function addons_dir(){
            return self::plugin_dir() . 'addons/';
        }
        static function addons_url(){
            return self::plugin_url() . 'addons/';
        }
        static function widgets_url(){
            return self::addons_url() . 'widgets/';
        }
        static function widgets_dir(){
            return self::addons_dir() . 'widgets/';
        }
        static function upload_dir(){
            $path = wp_upload_dir();
            return $path['basedir'];
        }
        static function upload_url(){
            $path = wp_upload_dir();
            return $path['baseurl'];
        }
        public function __construct() {
            add_action( 'admin_enqueue_scripts', array($this,'load_admin_style' )); //load admin styles
            add_action( 'elementor/editor/after_enqueue_styles', array( $this, 'load_admin_style' ) );
            add_action( 'plugins_loaded', function (){
                require_once self::plugin_dir().'/classes/notice.php'; //Elementor Missing Notice Code
            } );
            require_once self::plugin_dir().'/addons/widgets.php'; // widgets register File
            add_action('elementor/elements/categories_registered','master_addon_categories'); //Register Cutom Addon Categories
            add_action( 'elementor/widgets/widgets_registered',  'master_init_widgets' ); //register widgets
            add_action( 'elementor/init', function(){
                require_once self::plugin_dir().'/handler.php';
            });

            if (!did_action( 'elementor/loaded' )) { // Check if Elementor installed and activated
                add_action( 'admin_notices', [ $this, 'missing_elementor_notice' ] ); //show message
                return;
            }
            add_action( 'elementor/element/after_section_end', function( $section, $section_id ) {


                if ( 'section_advanced' === $section_id || '_section_style' === $section_id ) {

                    //Start Custom Settings Section
                    $section->start_controls_section(

                        'opt_css',
                        [
                            'label' => __( 'MasterElements CSS', 'masterelements' ),
                            'tab' => \Elementor\Controls_Manager::TAB_ADVANCED,

                        ]
                    );

                    $section->add_control(
                        'front_note',
                        [
                            'type' => \Elementor\Controls_Manager::RAW_HTML,
                            'raw' => __( 'This Feature Only Works on the frontend.', 'masterelements' ),
                            'content_classes' => 'elementor-descriptor',

                        ]
                    );

                    $repeater = new \Elementor\Repeater();
                    $repeater->add_control(
                        'break_title',
                        [
                            'label' => __( 'Title', 'masterelements' ),
                            'type' => \Elementor\Controls_Manager::TEXT,
                            'default' => __( 'Default title', 'masterelements' ),
                            'placeholder' => __( 'Type your title here', 'masterelements' ),
                        ]
                    );

                    $repeater->add_control(
                        'break_enable',
                        [
                            'label' => __( 'Enable Media Query', 'masterelements' ),
                            'type' => \Elementor\Controls_Manager::SWITCHER,
                            'label_on' => __( 'Show', 'masterelements' ),
                            'label_off' => __( 'Hide', 'masterelements' ),
                            'return_value' => 'yes',
                            'default' => 'yes',
                        ]
                    );

                    $repeater->add_control(
                        'show_min',
                        [
                            'label' => __( 'Show Min', 'masterelements' ),
                            'type' => \Elementor\Controls_Manager::SWITCHER,
                            'label_on' => __( 'Show', 'masterelements' ),
                            'label_off' => __( 'Hide', 'masterelements' ),
                            'return_value' => 'yes',
                            'default' => 'yes',
                            'condition' => [
                                'break_enable' => 'yes',
                            ]
                        ]
                    );

                    $repeater->add_control(
                        'show_max',
                        [
                            'label' => __( 'Show Max', 'masterelements' ),
                            'type' => \Elementor\Controls_Manager::SWITCHER,
                            'label_on' => __( 'Show', 'masterelements' ),
                            'label_off' => __( 'Hide', 'masterelements' ),
                            'return_value' => 'yes',
                            'default' => 'yes',
                            'condition' => [
                                'break_enable' => 'yes',
                            ]
                        ]
                    );
                    $repeater->add_control(
                        'break_min',
                        [
                            'label' => __( 'Min Width (px)', 'masterelements' ),
                            'type' => \Elementor\Controls_Manager::NUMBER,
                            'min' => 5,
                            'max' =>1000,
                            'step' => 5,
                            'default' => 0,
                            'condition' => [
                                'show_min' => 'yes',
                                'break_enable' => 'yes',
                            ]
                        ]
                    );
                    $repeater->add_control(
                        'break_max',
                        [
                            'label' => __( 'Max Width (px)', 'masterelements' ),
                            'type' => \Elementor\Controls_Manager::NUMBER,
                            'min' => 5,
                            'max' => 3000,
                            'step' => 5,
                            'default' => 100,
                            'condition' => [
                                'show_max' => 'yes',
                                'break_enable' => 'yes',
                            ]
                        ]
                    );
                    $repeater->add_control(
                        'custom_css',
                        [
                            'type' => \Elementor\Controls_Manager::CODE,
                            'label' => __( 'Add Your Own Custom Css Here', 'masterelements' ),
                            'language' => 'css',
                            'render_type' => 'ui',
                            'show_label' => false,
                            'separator' => 'none',
                        ]
                    );
                    $repeater->add_control(
                        'css_description',
                        [
                            'raw' => __( 'Use "selector" to target wrapper element. Examples:<br>selector {color: red;} // For main element<br>selector .child-element {margin: 10px;} // For child element<br>.my-class {text-align: center;} // Or use any custom selector', 'masterelements' ),
                            'type' => \Elementor\Controls_Manager::RAW_HTML,
                            'content_classes' => 'elementor-descriptor',
                        ]
                    );
                    $section->add_control(
                        'breakpoints_list',
                        [
                            'label' => __( 'Add Your Own Custom Css Here', 'masterelements' ),
                            'type' => \Elementor\Controls_Manager::REPEATER,
                            'fields' => $repeater->get_controls(),
                            'default' => [
                                [
                                    'break_title' => __( 'Title #1', 'masterelements' ),
                                    'break_enable'=>__( 'yes', 'masterelements' ),
                                    'show_min'=>__( 'yes', 'masterelements' ),
                                    'show_max'=>__( 'yes', 'masterelements' ),
                                    'break_min' => __( 'Min', 'masterelements' ),
                                    'break_max' => __( 'Max', 'masterelements' ),
                                    'custom_css' => __( '', 'masterelements' ),
                                ],
                            ],
                            'title_field' => '{{{ break_title }}}',
                        ]
                    );
                    #End Custom Settings Section
                    $section->end_controls_section();
                }
            }, 10, 2 );
            //Control register for backend
            add_action( 'elementor/frontend/before_render', function (\Elementor\Element_Base $section ) {

                $dir = SELF::upload_dir()."/masterelements";
                if( is_dir($dir) === false )
                {
                    mkdir($dir);
                }
                $text='';
                foreach (  $section->get_settings_for_display('breakpoints_list') as  $item ) {
                    if($item{'break_enable'})
                    {
                        if($item['show_min']=='yes')
                        {
                            $text = '
											@media screen and (min-width:' .$item['break_min'].'px){'
                                .$item['custom_css'].
                                '}';
                        }

                        elseif($item['show_max']=='yes')
                        {
                            $text = '
											@media screen and (max-width:' .$item['break_max'].'px){'
                                .$item['custom_css'].
                                '}';
                        }
                        if($item['show_min']=='yes' && $item['show_max']=='yes'){
                            $text = '
											@media  (min-width:' .$item['break_min'].'px) and (max-width:' .$item['break_max'].'px){'
                                .$item['custom_css'].
                                '}';
                        }
                    }
                    else
                    {
                        $text = $item['custom_css'];
                    }
                    if($item['break_min']==='Min' || $item['break_max']==='Max' || $item['custom_css']==='')
                    {
                    }
                    else
                    {
                        $filename = $dir."/post-".$item['_id'].".css";
                        $fh = fopen($filename, "w");
                        fwrite($fh, $text);
                        wp_enqueue_style( 'custom-breakpoint'.$item['_id'],SELF::upload_dir().'/masterelements/post-'.$item['_id'].'.css', true,true );
                        fclose($fh);
                    }
                }
            } ,11 );

        }
        function load_admin_style()
        { //function to load admin style
            wp_enqueue_style( 'notics_style',self::assets_url().'/css/notics-style.css', null, self::version,'all' );
            wp_register_style( 'fontawesome',  \MasterElements::assets_url() . 'css/fontawesome.min.css', [], '1.0.1' );
            if(is_admin() ){ //check if its a admin interface and style is enqueued already

                wp_enqueue_style( 'admin_style',self::assets_url().'/css/admin_style.css', null, self::version,'all' );
            }
            if(is_admin()){ //check if its a admin interface and script is enqueued already

                wp_enqueue_script( 'admin_js',self::assets_url() .'/js/admin_style.js', null, self::version,'all' );

            }
        }
        function missing_elementor_notice() { //function to show elementor missing meesage

            if ( file_exists( WP_PLUGIN_DIR . '/elementor/elementor.php' ) ) {
                $btn['label'] = esc_html__('Activate Elementor', 'masterelements');
                $btn['url'] = wp_nonce_url( 'plugins.php?action=activate&plugin=elementor/elementor.php&plugin_status=all&paged=1', 'activate-plugin_elementor/elementor.php' );
            } else {
                $btn['label'] = esc_html__('Install Elementor', 'masterelements');
                $btn['url'] = wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=elementor' ), 'install-plugin_elementor' );
            }
            \MasterElements\Notice::sendParams(
                [

                    'type'        => 'error',
                    'dismissible' => true,
                    'btn'		  => $btn,
                    'message'     => '',
                ]
            );
        }

    };
    new MasterElements(); //load one instance of MasterElements class
endif;
?>